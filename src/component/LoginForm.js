import React from 'react';
import {
    Card, CardBody, Modal, Button, ModalHeader, ModalBody,
    ModalFooter, Form, FormGroup, Label, Input, Alert
} from 'reactstrap';
import ReCAPTCHA from "react-google-recaptcha";

const initialState = {
    username: "",
    password: "",
    isVerified: false,
    token: "",
    loginStatus: "",
    counterAlert: 30,
    showAlert: false,
    idleCounter: 30,
    showModal: false,
}

class LoginForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = initialState;
    }

    showInfo = false;
    loginAttempt = 3;

    handleInput(event) {
        this.resetIdleCounter()
        const name = event.target.name;
        const value = event.target.value;
        this.setState({ [name]: value });
    }

    closeModal(event) {
        this.resetIdleCounter()
        this.setState({ loginStatus: "None", showModal: false })
        this.showInfo = false;
    }

    login(event) {
        console.log(this.state);

        this.resetIdleCounter()

        event.preventDefault();
        const userValidate = {
            username: "user",
            password: "password01"
        }
        if (this.state.username == userValidate.username &&
            this.state.password == userValidate.password) {
            console.log('berhasil login');
            this.setState({ loginStatus: "Success" });
        } else {
            console.log('gagal login');
            this.setState({ loginStatus: "Failed" });
            // set logi  attempt
            this.loginAttempt = this.loginAttempt - 1;
            console.log('existing attempt = ' + this.loginAttempt);

            if (this.loginAttempt == 0) {
                this.setState({ showAlert: true, counterAlert: 30 });
                this.loginInterval = setInterval(() => {
                    const prevCount = this.state.counterAlert

                    if (prevCount == 0) {
                        this.setState({ showAlert: false });
                        clearInterval(this.loginInterval)
                    } else {
                        this.setState(prevState => ({ counterAlert: prevState.counterAlert - 1 }))
                    }
                }, 1000)
            }

        }
        this.showInfo = true

        event.target.reset();

    }

    handleVerify(token) {
        this.setState({ isVerified: true, token: token })
        this.resetIdleCounter()
    }

    idleCounter() {
        const idleInterval = setInterval(() => {
            const prevCount = this.state.idleCounter;

            if (prevCount == 0) {
                // this.setState({showModal: true})
                console.log("idle for 30 secs");
                clearInterval(idleInterval);
                this.setState({ showModal: true, idleCounter: 30 });
            } else {
                this.setState(prevState => ({ idleCounter: prevState.idleCounter - 1 }))
            }

        }, 1000)
    }

    resetIdleCounter() {
        this.setState({ idleCounter: 30 })
    }

    componentDidMount() {
        console.log("component did mount");
        this.idleCounter()

    }

    componentDidUpdate(data) {
        console.log(this.state);

    }


    render() {

        return (
            <div>
                <div className="login-container">
                    <Card>
                        <CardBody>
                            <Form className="form" onSubmit={(e) => {
                                this.login(e)
                            }}>
                                <h3>Login</h3>
                                <FormGroup>
                                    <Label for="usernameField">Username</Label>
                                    <Input
                                        type="text"
                                        name="username"
                                        id="usernameField"
                                        placeholder="Username"
                                        onChange={(e) => {
                                            this.handleInput(e)
                                        }}
                                        required
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="passwordField">Password</Label>
                                    <Input
                                        type="password"
                                        name="password"
                                        id="passwordField"
                                        placeholder="********"
                                        onChange={(e) => {
                                            this.handleInput(e)
                                        }}
                                        required
                                    />
                                </FormGroup>
                                <ReCAPTCHA
                                    sitekey="6LfokH8iAAAAAAhldFQnq8COakObmSjJi3htzO2y"
                                    onChange={(e) => {
                                        this.handleVerify(e)
                                    }}
                                    required
                                />
                                <Button color="primary">LOGIN</Button>
                            </Form>

                            <Alert color="info" className="mt-3" isOpen={this.state.showAlert} >
                                Please wait {this.state.counterAlert} seconds to log back in
                            </Alert>
                            <span>Idle counter : {this.state.idleCounter}</span>
                        </CardBody>
                    </Card>
                </div>

                <Modal isOpen={this.state.showModal}>
                    <ModalHeader>Information Pop Up Dialog</ModalHeader>
                    <ModalBody>
                        You have been idle for 30 seconds
                    </ModalBody>
                    <ModalFooter>
                        <Button color="danger"
                            onClick={(e) => {
                                this.closeModal(e)
                            }}>
                            Close</Button>
                    </ModalFooter>
                </Modal>

                <Modal isOpen={this.showInfo}>
                    <ModalHeader>Information Pop Up Dialog</ModalHeader>
                    <ModalBody>
                        <p>
                            Your login status : {this.state.loginStatus}<br />

                            {
                                this.state.loginStatus == 'Success' ?
                                    <span >Welcome {this.state.username} !!!</span> : <span color="warning">Invalid username or password</span>
                            }



                        </p>

                    </ModalBody>
                    <ModalFooter>
                        <Button color="danger"
                            onClick={(e) => {
                                this.closeModal(e)
                            }}>Close</Button>
                    </ModalFooter>
                </Modal>

            </div>

        );
    }
}

export default LoginForm;